#!/bin/sh

# yt.sh 2.0
# Copyright (C) 2024 - 2025 the author indicated below
# The author of yt.sh made an OpenPGP,
# RSA key pair. The fingerprint of this key pair is
# BA34F30AC917CB0714884A3DA6BDBF5757B731E9
# yt.sh is distributed under the terms of the GNU General
# Public License, version 3 (https://www.gnu.org/licenses/gpl.html).
# yt.sh is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY--without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

if test -z "$(which zenity 2> /dev/null)"
then
  if which kdialog > /dev/null 2> /dev/null
  then
    kdialog --error "Zenity is not available." --title "Error"
  elif which xmessage > /dev/null 2> /dev/null
  then
    printf "Zenity is not available." | xmessage -center -buttons OK -default OK -xrm '*message.scrollVertical: Never' -file -
  fi
  exit 0
fi

if lsof -ni | grep "^tor .*127.0.0.1:9150 (LISTEN)$" > /dev/null
then
  torproxyport="9150" # Tor Browser
else
  torproxyport="9050" # MinusBrowser
fi

thisdir="$(dirname "$(readlink -e "${0}")")"
if test -f "${thisdir}/yt-dlp"
then
  ("${thisdir}/yt-dlp" --proxy socks5://127.0.0.1:${torproxyport} -U 2> /dev/null) | zenity --progress --text="Making sure yt-dlp is up to date..." --pulsate --auto-close --no-cancel
else
  (curl -sL --socks5-hostname 127.0.0.1:${torproxyport} "https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp" -o "${thisdir}/yt-dlp" 2> /dev/null) | zenity --progress --text="Downloading yt-dlp..." --pulsate --auto-close --no-cancel
  if test -f "${thisdir}/yt-dlp"
  then
    chmod 755 "${thisdir}/yt-dlp"
  else
    zenity --error --width=400 --title "Download Failed" --text="Download of yt-dlp failed."
  fi
fi
if ! test -f "${thisdir}/yt-dlp" && test -z "$(which yt-dlp 2> /dev/null)"
then
  zenity --error --width=400 --title "Error" --text="yt-dlp is not available."
  exit 0
fi
if test -z "$(which xterm 2> /dev/null)"
then
  zenity --error --width=400 --title "Error" --text="XTerm is not available."
  exit 0
fi
if test -z "$(which mpv 2> /dev/null)"
then
  zenity --error --width=400 --title "Error" --text="MPV is not available."
  exit 0
fi

if ! test -x "/usr/local/bin/tails-about" && ! zenity --question --width=400 --title="Use Tor?" --text="Use Tor? With Tor you have much more privacy, but YouTube can be Tor-unfriendly." --ok-label="Use Tor" --cancel-label="Without Tor"
then
  torproxyport="" # no Tor
fi

if zenity --question --width=400 --title="Stream or Save?" --text="Stream the video or save it?" --ok-label="Save" --cancel-label="Stream"
then
  minusdir="$(dirname "${thisdir}")"
  downloads="${minusdir}/downloads"
  if which "caja" > /dev/null 2> /dev/null
  then
    caja "${downloads}" 2> /dev/null
  elif which "nemo" > /dev/null 2> /dev/null
  then
    nemo "${downloads}" 2> /dev/null
  elif which "pcmanfm" > /dev/null 2> /dev/null
  then
    pcmanfm "${downloads}" 2> /dev/null
  elif which "thunar" > /dev/null 2> /dev/null
  then
    thunar "${downloads}" 2> /dev/null
  elif which "dolphin" > /dev/null 2> /dev/null
  then
    dolphin "${downloads}" 2> /dev/null
  elif which "nautilus" > /dev/null 2> /dev/null
  then
    nautilus "${downloads}" 2> /dev/null
  fi
  sleep 2
  cd "${downloads}"
  if test -n "${torproxyport}"
  then
    env PATH="${thisdir}:${PATH}" xterm -e yt-dlp --proxy socks5://127.0.0.1:${torproxyport} -f "[width<1090]" "${1}" 2> /dev/null &
  else
    env PATH="${thisdir}:${PATH}" xterm -e yt-dlp -f "[width<1090]" "${1}" 2> /dev/null &
  fi
else
  if test -n "${torproxyport}"
  then
    env PATH="${thisdir}:${PATH}" xterm -e mpv --volume=30 --volume-max=200 --ytdl-raw-options=proxy=[socks5://127.0.0.1:${torproxyport}] --ytdl-format="[width<1090]" --player-operation-mode=pseudo-gui -- "${1}" 2> /dev/null &
  else
    env PATH="${thisdir}:${PATH}" xterm -e mpv --volume=30 --volume-max=200 --ytdl-format="[width<1090]" --player-operation-mode=pseudo-gui -- "${1}" 2> /dev/null &
  fi
fi

exit 0
