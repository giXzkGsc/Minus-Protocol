# Minus Protocol Project

## A Brief Introduction to the Minus Protocol

The name Minus was inspired by Gopher. Gopher Plus added features to Gopher, but I wanted to subtract features. I wanted a Gopher Minus. I shortened this to Minus.

Minus is about as simple, secure, and privacy-respecting as a file service protocol can possibly be.

A Minus client sends one line of plain text to the server that specifies the file desired, and the Minus server either sends back the file or an error message in UTF-8 text. There is nothing like request and response headers and nothing like cookies. The server software does not identify itself to the client, and the client does not identify itself to the server. The client opens the TCP connection, and the server closes it. No other communication is allowed between client and server.

The Minus analog of a Web Site, Gopher Hole, or Gemini Capsule is the Minus Library. Minus Libraries are a way to make media available to be perused. As with public libraries, users are not required to identify themselves before they are allowed to peruse media. Minus does not prevent impersonation; it prevents identification in order to protect privacy.

When a Minus client creates or alters a file on the device it runs on, it does so only when the user explicitly causes it to. The server can not store any information on the client in any way, except a file that the client requests from the server. Similarly, the client can not cause any information to be stored on the server by any means. The server can keep a request log, but is not required to. Because Tor is always used, this log can not include the IP addresses of clients. It can only include the files requested and the times they were requested.

The files served can be of any type, but only `.minus`, `.txt`, `.text`, and `.asc` files will be displayed by the client without first being saved to mass storage.

All Minus servers that serve files over the Internet are required to run as Tor Onion Services. Minus clients should have Tor built in the way that Tor Browser does. Tor provides transport security and allows Minus to work without certificate authorities or domain registrars. It also prevents clients and servers from learning each other's IP addresses.

Minus (`.minus`) files are UTF-8 text. Markings similar to Markdown are allowed but not required. Clients are allowed to ignore markings, but they are meant to indicate parts of the file and the language (e.g., French) of the file. How text is displayed by the client is controlled by the client and its user. The intention is to take power away from the author and give it to the reader.

Markings are not allowed to be hidden from the user. Nothing in the file is allowed to be hidden from the user.

Minus files can contain hyperlinks to other files. Any URL alone on a line is automatically a link. There are no "inline" links, and no relative links to files on the Internet.

Minus defines its own MIME type, like HTTP's `text/html`. This is `text/minus`, and the file name suffix is `.minus`.

Minus URLs are of the form `minus://domain.onion/something.minus`. There is no optional authority component, nor are there any optional query or fragment components. The browser sends the part of the URL after the `.onion` to the server to specify the desired file.

## The Software

MinusBrowser is a browser for the Minus Protocol. It runs on Linux-based operating systems. MinusBrowser includes a built-in Minus server. With just two button clicks, you will be able to start serving a Minus library that anyone in the world with a copy of MinusBrowser can read. Minus pages are easy to write--much, much easier than HTML.

The current version of MinusBrowser is 2.46.1.

MinusBrowser is written in Tcl/Tk, and it also requires `curl`. Like the Tor Browser, MinusBrowser includes its own copy of Tor.

There is no need to install the browser. Just download the `.tar.gz` archive and unpack it. The `ReadMeFirst.txt` file explains how to launch it on various distros.

https://codeberg.org/giXzkGsc/Minus-Protocol/raw/branch/main/MinusBrowser.tar.gz

#### Latest Updates

Version 2.46.1 of MinusBrowser.

- The latest version of `yt.sh`, which you can now download with a link on MinusBrowser's home page, allows MinusBrowser to play videos without a web browser. It requires MPV, Zenity, and XTerm. This latest version of `yt.sh` downloads the latest version of `yt-dlp` to the MinusBrowser folder and keeps it updated. New versions of `yt-dlp` have to be published often. The version usually installed with `mpv` will not play or save most YouTube videos because it is about 3 years out of date.

Version 2.46 of MinusBrowser.

- Updated to Tor 0.4.8.14.
- Added some information to the `about:browser` page.
- Specifying a port in a URL when the protocol is not Minus is no longer considered an error. This fixes a bug in previous versions.
- In Gopher menus, type 3 items (which indicate an error), instead of being represented with a useless link, are now represented with the word "Error." This fixes a bug in previous versions.
- Previous versions of Tor were released under the "3-clause BSD" license. The builds of Tor used by MinusBrowser are now released under the GNU General Public License, version 3. The license information included with MinusBrowser has been revised accordingly.
- The copyright years have been fixed in the copyright notices of some of the files.

Version 2.45 of MinusBrowser.

- Information on the `about:browser` window has been rearranged a bit.
- Many more comments have been added, especially to `download-url.sh`, to make MinusBrowser easier to maintain and fork.
- The specification for `URL:` links in Gopher it now bundled with MinusBrowser rather than simply linked.
- The server window now closes when you type Control-q, along with all the browser windows. Only the built-in text editor windows still have to be closed individually.
- When you try to open a second server window, MinusBrowser now tells you that the server window is already open.
- MinusBrowser now handles the unlikely possibility that the contents of the three settings files, `easygpg.txt`, `theme.txt`, and `log-file-path.txt` may be unusable. It corrects these to something usable. This fixes a bug in previous versions.
- The permissions of all the files, except Tor, its libraries, and `mini-inetd`, have been changed so that only the owner can edit them.
- Links to `https://youtube.com/shorts/` videos are now handled, as well as links to `https://www.youtube.com/shorts/` links.

Version 2.44.1 of MinusBrowser.

- The `about:server` page now handles the possibility that the `file-urls.minus` file might not exist, even though the server is running. This fixes a bug in previous versions.
- The `about:computer` page now handles the possibility that a Default Language may not be set in the locale. This fixes a bug in previous versions.
- MinusBrowser previously handled `URL:` links in Gopher type 1 and 7 files by deleting all of the link up to and including the `URL:`, thus avoiding the need to handle these links. However, such links can occur in other types of files, even those read with other protocols. Now the link is displayed unchanged, but all of it, up to and including the `URL:`, is ignored when the link is read. This fixes a bug in previous versions.
- MinusBrowser will now handle links that begin with `https://youtube.com/shorts/`, as well as those that begin with `https://www.youtube.com/shorts/`.
- Several new comments have been added to some of the files to make it easier for me to maintain MinusBrowser and easier for others who may want to fork it.

Version 2.44 of MinusBrowser.

- Some of the buttons on the browser window have moved.
- MinusBrowser now works around a problem with the `about:computer` page on some distros.
- The `about:browser` page now includes the versions of `mpv` and `yt-dlp` if these are used by `yt.sh`, which plays videos.
- Some software replaces some characters in `file://` URLs with sequences that begin with `%`. For example, they may replace `"` with `%22`. MinusBrowser does not do this, but you may want to use files created by other software that does this. MinusBrowser now makes this easier by recognizing the sequences `%20` through `%2C`.
- The Minus Protocol Specification has been edited to provide a little more clarity. The protocol itself has not changed. It may never change again.
- The documentation of software licenses for `mini-inetd`, Tor, and libraries included with Tor has been improved.

Version 2.43.3 of MinusBrowser.

- Fixed a bug that allowed some bogus URLs to crash MinusBrowser. This fixes a bug in previous versions.
- Fixed a mistake in the `ReadMeFirst.txt` file. This fixes a bug in previous versions.

Version 2.43.2 of MinusBrowser.

- A bug that prevents proper updating has been fixed. This fixes a bug in the previous version.

Version 2.43.1 of MinusBrowser.

- MinusBrowser will now handle videos on `media.ccc.de/v/` (Chaos Computer Club) as well as videos on YouTube, YewTube, Framatube, and Undernet.
- The built-in Minus server no longer reports that a file is not in the library when it should report that the URL is bad. MinusBrowser will never allow bad URLs to be requested from the server, but there may be other software that will. This fixes a bug in previous versions.
- MinusBrowser now correctly handles all `file://` URLs that specify files that do not exist or that you do not have permission to read, and not just those in links. This fixes a bug in previous versions.
- Fixed a mistake on the Home Page. The status display is to the left of the "Home Page" button, not the "Bookmark page" button. This fixes a bug in previous versions.
- MinusBrowser now works around a problem with some symbolic links to directories in `/usr`. This fixes a bug in previous versions.
- MinusBrowser can now list items in directories in `/dev`. This fixes a bug in previous versions.
- MinusBrowser now correctly handles URI schemes with unexpected characters, including `-`. This fixes a bug in previous versions.

Version 2.43 of MinusBrowser.

- MinusBrowser now tests to see if Tails is the OS by checking for `/usr/local/bin/tails-about` instead of `/usr/local/bin/tails-version`. Because `tails-version` is deprecated, it may disappear.
- `ipaddress.sh` is no longer mentioned or given special support. It does not seem entirely reliable, and it is redundant. `ipleak.net` gives the same information and more.
- The PeerTube instances `framatube.org` and `tube.undernet.uy` are now supported just as the various YouTube and YewTube URLs are.
- `file://` URLs that specify files that do not exist or contain `%20` are now correctly handled. This fixes a bug in previous versions.
- MinusBrowser now determines the Internet gateway by looking at the route to `9.9.9.9` (QUAD9) instead of `93.184.215.14` (the IP address of example.com) because QUAD9 seems less likely to change its address.
- A few new items of information have been added to the `about:` pages, and some information has been moved.

Version 2.42.1 of MinusBrowser.

- Replaced backticks in `download-url.sh` with `"$()"`. This is better style.
- Local addresses with ports specified are no longer identified as bad domain names. This fixes a bug in previous versions.

Version 2.42 of MinusBrowser.

- Added uptime and memory statistics to the `about:computer` page.
- Added downloads folder contents to the `about:browser` page.
- Added support for `youtube.com/live` links.
- Corrected the check for valid domain names when downloading files. This fixes a bug in previous versions.
- Moved the "A Brief Introduction to the Minus Protocol" from the Home Page to a separate file linked on the Home Page.
- Added Port 70 News to the "Known Minus Libraries and Selected Gopher Holes" page.
- Changed some colors in all three themes.
- Allowed pages to be bookmarked even if they are already on the "Known Minus Libraries and Selected Gopher Holes" page, after warning that they are already bookmarked.
- Added Yewtube to notes about YouTube on the protocols and schemes page.
- When the built-in Minus server's index is checked, specifiers that begin with `/-` or `-` are now rejected. This fixes a bug in previous versions.
- Checking of specifiers by the built-in Minus server now checks for every possible error. This fixes a bug in previous versions.
- Looking up path names by the built-in Minus server now accounts for very unlikely edge cases. This fixes a bug in previous versions.

Version 2.41 of MinusBrowser.

- Updated to Tor 0.4.8.13. This fixes a bug in the previous version of Tor.
- Shortened the Tor section of the status display.
- Fixed a bug in the bookmarking of pages in the MinusBrowser folder. This fixes a bug in previous versions.
- Simplified the bookmarking code, and added code that prevents bookmarking pages already bookmarked.
- Replaced the "Add to menu" button with a "Bookmark page" button. Typing Shift-Control-m is now the only way to add MinusBrowser to the Applications menu. Pages are bookmarked more often than MinusBrowser is added to the menu.
- The "Home Page" button is now next to the "Server" button, and the "Bookmark page" button is where the "Add to menu" button used to be.

Version 2.40 of MinusBrowser.

- MinusBrowser now works around a bug in some old versions of `cal`.
- MinusBrowser now behaves properly when a download of a file that is not plain text fails. This fixes a bug in previous versions.
- `file://minusupdate.tar.gz` is now displayed in the URL area of the window after a MinusBrowser update is downloaded and verified. Previously, it displayed `file://minusupdate.tar.gz.gpg`. This fixes a bug in previous versions.
- The built-in copy of the Minus Protocol Specification has been revised to add more details. These changes should not require any changes to Minus client or server software that you wrote while following the previous version of the Specification.

Version 2.39.3 of MinusBrowser.

- MinusBrowser now checks to make sure the `cal` command is available before it attempts to display `about:` pages with calendars. Tails does not have `cal` or `ncal` installed by default. It is possible that other distros have this same problem. This fixes a bug in recent versions.

Version 2.39.2 of MinusBrowser.

- Fixed error handling of `about:` pages for out-of-range years. This fixes a bug in previous versions.
- Typing Shift-Control-t now displays the `about:today` page.
- Typing Shift-Control-m is now equivalent to clicking the "Add to menu" button. Now, once again, all buttons have keyboard equivalents.

Version 2.39.1 of MinusBrowser.

- Capital letters are now allowed in domain names. This fixes a bug in previous versions.

Version 2.39 of MinusBrowser.

- Updated OpenSSL (used by Tor) to 3.0.15.
- Added an `about:today` page with today's date and time and a calendar for the current month, the previous 5 months, and the next 6 months.
- Added about pages for each year from 1 to 9999. For example, the calendar for this year is `about:2024`.
- MinusBrowser now behaves properly when you try to view a file or folder that you do not have permission to view. This fixes a bug in previous versions.
- The Minus Protocol Specification has been edited to change `loopback` to `localhost`. This corrects a typographical error.

Version 2.38 of MinusBrowser.

- The `about:computer` page now displays information about mounted volumes.
- The built-in documentation now includes RFC 1436 (The Internet Gopher Protocol) and RFC 4266 (The Gopher URI Scheme).
- When MinusBrowser displays a man page, it now also displays error messages (if any) from the `man` command.
- Handling of links is simpler now. One result of this is that all URLs alone on a line are links, even when the protocol is not one that MinusBrowser currently supports (e.g., `gemini://` and `ftps://`). Clicking on a link with an unsupported protocol simply displays an error message.

Version 2.37.2 of MinusBrowser.

- MinusBrowser now once again properly handles downloading files other than text files. This fixes a bug in the previous 5 versions.

Version 2.37.1 of MinusBrowser.

- When you click on a Gopher type 7 link (a Gopher search service), MinusBrowser now copies the URL to the text entry area and appends `%09` instead of `?` to conform to RFC 4266. `?` worked with most, but not all services. This fixes a bug in previous versions.

Version 2.37 of MinusBrowser.

- The `about:browser` page now includes the versions of the software used to read pages aloud, as well as the versions of other software used by MinusBrowser.
- The `about:network` page now includes more information.
- MinusBrowser can now extract information about IP addresses and domain names from the web page returned by `ipleak.net`. Enter or select an IP address or domain name and then type Shift-Control-i. If you type Shift-Control-i when nothing is entered or selected, you will see information about your current Tor exit node.

Version 2.36.1 of MinusBrowser.

- Any `.` at the beginning of a default file name should now be stripped out for all files, not just some. This fixes a bug in the previous version.

Version 2.36 of MinusBrowser.

- The `.bash_history` file now appears on the "Read file" page. This provides an easy way to search and copy from the history.
- Non-alphanumeric characters in `file://` links are now better handled. This fixes a bug in previous versions.
- `yewtu.be` URLs can now be played in the same way that `www.youtube.com` URLs are played.
- If a `?` appears in a YouTube or YewTube URL, the `?` and everything that follows it is stripped out. This prevents YouTube's tracking.
- Any `.` at the beginning is stripped out of the default file name of a file before it is saved.

Version 2.35.1 of MinusBrowser.

- MinusBrowser will no longer try to read a URL from the Internet that does not have a proper domain name. This fixes a bug in previous versions.

Version 2.35 of MinusBrowser.

- If the server request log is not set to `/dev/null`, and the log file does not already exist, an empty log file will be created when the server is started. Previously, the log file was not created until the first request was logged.
- When MinusBrowser creates `file://` links to files in your MinusBrowser folder, it now uses relative links (relative to the MinusBrowser folder) whenever possible.
- The `about:server` page now includes a local (`localhost`) link to your library's `index.minus` page.

Version 2.34.1 of MinusBrowser.

- Until this version, when you updated MinusBrowser, you would see a page with details about the update (`minusupdate.tar.gz.gpg`). This is inappropriate because, by the time these details are displayed, the file is already replaced (with `minusupdate.tar.gz`) or removed. Now, instead of the details, "MinusBrowser Update" is displayed. This fixes a bug in previous versions.
- MinusBrowser now behaves correctly when the version of EasyGPG can not be displayed because it is no longer where the file `easygpg.txt` says it is. This fixes a bug in previous versions.
- Some unnecessary text has been removed from the Minus Protocol Specification. This does not make it necessary for any changes to be made to software that already followed the specification before this text was removed. It is my hope that no changes in the Specification will be required in the future that would force changes in Minus software that follows the Specification as currently written.

Version 2.34 of MinusBrowser.

- Updated to Tor 0.4.8.12.
- The `about:browser` and `about:server` pages now show MinusBrowser's connections to the Tor network.

Version 2.33 of MinusBrowser.

- MinusBrowser now supports URLs that begin with `https://www.youtube.com/shorts/`, as well as `https://www.youtube.com/watch?v=`, `https://youtu.be/` and `https://www.youtube.com/embed/`.
- The `about:` pages now make it easy to edit all the settings files: browser theme, EasyGPG location, and server request log location.

Version 2.32 of MinusBrowser.

- MinusBrowser's built-in Minus server has a problem with file specifiers that begin with `-`, so this is no longer allowed. MinusBrowser and its built-in server have been changed to enforce this, and the Minus Protocol Specification has been revised accordingly. I hope this will be the last revision to the specification.
- When files are read instead of Internet URLs, `file://` URLs now appear in the URL area of the browser window, instead of the full path name of the file.
- MinusBrowser will no longer attempt to display text files larger than 50MiB, in order to protect the text widget of Tk from being overloaded. Files downloaded from the Internet were already truncated to 50 MiB for the same reason.

Version 2.31 of MinusBrowser.

- MinusBrowser now reads weather reports from `wttr.in`. Enter or select a location, and then type Shift-Control-w. You can specify the location in several different ways. Locations can be anywhere in the world, not just in the USA. Search the Home page for `weather` to read more about this.
- Improved the `about:network` page.
- Improved the filtering of text pages read by MinusBrowser. This fixes a bug in previous versions.
- Changed the color of found text in the `dark` theme to improve the contrast.

Version 2.30.1 of MinusBrowser.

- MinusBrowser now correctly displays the Tor version in the `about:browser` and `about:server` pages. This fixes a bug in the previous two versions.

Version 2.30 of MinusBrowser.

- Updated to Tor 0.4.8.11.
- Clicking on a `file://` link to a missing file no longer fails. This fixes a bug in the previous version.
- The `about:` pages have been improved, and two more have been added: `about:computer` and `about:network`.
- The built-in documentation has been revised, and a new page has been added that summarizes the protocols and schemes supported by MinusBrowser. The three files: `schemes.minus`, `keycmd.minus`, and `clicks.minus` together now provide a quick reference for MinusBrowser.
- Typing `Shift-Control-j` now finds words with spellings similar to the entered or selected text. This is the same lookup used by the editor window when you select a word and type `Control-d`.

Version 2.29 of MinusBrowser.

- Added an `about:` scheme with three pages: `about`, `browser`, and `server`. This is similar to the `about:` scheme of some web browsers. Search for `about:` on the Home page for details. More pages will probably be added in the future.
- Links are now followed up to three levels deep.
- The very limited support for the Gemini protocol has been removed. This will probably be added back if, and when, `curl` supports the Gemini protocol.
- Some more cruft related to the way MinusBrowser used to work with the Tails OS has been removed.
- When files other than text files are read, a window with details about the file is displayed. You can delete these non-text files by typing Shift-Delete, just as with text files. A note about this now appears on the details page.
- MinusBrowser can now read any file or folder using EasyGPG, if EasyGPG is installed, by typing Control-d. Previously, this only worked with text files. If the file contains no PGP messages or keys, EasyGPG will offer to encrypt the file.
- More links now appear on the page that appears when you click "Read file" (or type Control-o).

Version 2.28.2 of MinusBrowser.

- When Internet connections fail, MinusBrowser no longer retries twice. This should prevent MinusBrowser from appearing to be frozen for a full minute.
- The page that appears when you click the "Read file" button will no longer include links to files that do not exist because you have never started your Minus server. This fixes a bug in previous versions.
- MinusBrowser now ensures that the `Tor` folders now have the proper permissions. Tor will not run if these are not correct. This has not been a problem, but there is no reason not to be more careful.

Version 2.28.1 of MinusBrowser.

- Fixed another bug for Tails users caused by changes in the latest version of Tails.

Version 2.28 of MinusBrowser.

- `Control-y` has been reassigned to search for the entered or selected word or phrase in Idiotbox.
- `Control-p` now searches for the entered or selected word or phrase in Veronica 2, the search engine for Gopherspace.
- Several key combinations that did not work correctly when the cursor was in the text entry area now work correctly. This fixes a bug in previous versions.
- Additional information is displayed about files read with MinusBrowser that are not text files. MinusBrowser now displays the type, size, and modification date of such files. For video and audio files, if `ffprobe` is available, tags will also be displayed. If `ffmpeg` is installed, `ffprobe` should be available.
- The pages opened by the "Read file" button (and Control-o) now include more links.
- Audacious is now preferred to VLC when reading a page aloud. Audacious is more appropriate because it only plays audio files. Newer versions of VLC also have more "glitches."
- Tails now no longer supports Desktop files at all. Read the `ReadMeFirst.txt` file to see how this changes the way we use MinusBrowser with Tails.

Version 2.27.2 of MinusBrowser.

- Updated OpenSSL to 3.0.13, and libcrypto.so.3 was also updated.

Version 2.27.1 of MinusBrowser.

- `MinusBrowser.sh` now checks for `xdg-open` instead of `xdg-mime`. This fixes a bug in previous versions.

Version 2.27 of MinusBrowser.

- MinusBrowser now requires Minus URLs to end with a file name extension like `.gz`, `.txt`, etc. The Minus Protocol Specification already specified that the file type is indicated by the extension, implicitly requiring an extension. Now the specification makes this requirement explicit, and MinusBrowser rejects file names without extensions.
- In addition to VLC and Audacious, MinusBrowser can now use MPV when reading pages aloud. The first choice is VLC, then Audacious, and then MPV.
- Because MinusBrowser can not play YouTube videos discovered with Idiotbox, it now plays them with other software (by default, the default web browser). Note that this may mean downloading content without using Tor. It is also possible to play these videos with software other than a web browser, if such software is available. Search the MinusBrowser home page for "youtube" to read more details.
- Some errors in built-in documentation have been fixed, and new information has been added.

### Communicating about Minus

For news and information about Minus, and to report bugs in my software, follow the #minus-protocol #tag on Diaspora. You can create a Diaspora account without giving up personal information.

https://nerdpol.ch/tags/minus-protocol

